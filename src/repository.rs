use crate::{
    database::Pool,
    model::{Todo, UpdateTodo},
};
use anyhow::Result;
use diesel::prelude::*;

pub struct Repository {
    db_pool: Pool,
}

impl Repository {
    pub fn new(db_pool: Pool) -> Self {
        Self { db_pool }
    }

    pub async fn get_by_id(&self, todo_id: i32) -> Result<Option<Todo>> {
        let pool = self.db_pool.clone();
        let todo = tokio::task::spawn_blocking(move || -> anyhow::Result<_> {
            use crate::schema::todos::dsl::*;
            let mut conn = pool.get()?;
            todos
                .filter(id.eq(todo_id))
                .get_result(&mut conn)
                .optional()
                .map_err(From::from)
        })
        .await??;

        Ok(todo)
    }

    pub async fn get_all(&self) -> Result<Vec<Todo>> {
        let pool = self.db_pool.clone();
        let todo = tokio::task::spawn_blocking(move || -> anyhow::Result<_> {
            use crate::schema::todos::dsl::*;
            let mut conn = pool.get()?;
            todos.get_results(&mut conn).map_err(From::from)
        })
        .await??;

        Ok(todo)
    }

    pub async fn insert_todo(&self, todo: Todo) -> Result<Todo> {
        let pool = self.db_pool.clone();
        let todo = tokio::task::spawn_blocking(move || -> anyhow::Result<_> {
            use crate::schema::todos::dsl::*;
            let mut conn = pool.get()?;
            diesel::insert_into(todos)
                .values(&todo)
                .returning(Todo::as_returning())
                .get_result(&mut conn)
                .map_err(From::from)
        })
        .await??;

        Ok(todo)
    }

    pub async fn delete_by_id(&self, todo_id: i32) -> Result<Todo> {
        let pool = self.db_pool.clone();
        let todo = tokio::task::spawn_blocking(move || -> anyhow::Result<_> {
            use crate::schema::todos::dsl::*;
            let mut conn = pool.get()?;
            diesel::delete(todos)
                .filter(id.eq(todo_id))
                .returning(Todo::as_returning())
                .get_result(&mut conn)
                .map_err(From::from)
        })
        .await??;

        Ok(todo)
    }

    pub async fn update_todo(&self, todo_id: i32, todo: UpdateTodo) -> Result<Todo> {
        let pool = self.db_pool.clone();
        let todo = tokio::task::spawn_blocking(move || -> anyhow::Result<_> {
            use crate::schema::todos::dsl::*;
            let mut conn = pool.get()?;
            diesel::update(todos)
                .filter(id.eq(todo_id))
                .set(todo)
                .returning(Todo::as_returning())
                .get_result(&mut conn)
                .map_err(From::from)
        })
        .await??;

        Ok(todo)
    }
}
