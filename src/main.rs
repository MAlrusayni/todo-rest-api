use tracing::{info, Level};
use tracing_subscriber::FmtSubscriber;

pub mod database;
pub mod filter;
pub mod model;
pub mod repository;
pub mod schema;
pub mod service;

#[macro_use]
extern crate diesel;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    envmnt::load_file(&format!("{}/.env", env!("CARGO_MANIFEST_DIR")))?;
    tracing()?;

    let pool = database::init_db().await?;
    let todo_api = filter::api(pool);

    warp::serve(todo_api).run(([127, 0, 0, 1], 3030)).await;

    Ok(())
}

pub fn tracing() -> anyhow::Result<()> {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();

    tracing::subscriber::set_global_default(subscriber)?;
    info!("Tracing: ON");

    Ok(())
}
