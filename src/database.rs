use diesel::{
    pg::Pg,
    r2d2::{self, ConnectionManager},
};

pub type Pool = r2d2::Pool<ConnectionManager<diesel::PgConnection>>;

pub async fn init_db() -> anyhow::Result<Pool> {
    let manager = ConnectionManager::new(envmnt::get_or_panic("DATABASE_URL"));
    let pool =
        tokio::task::spawn_blocking(move || Pool::builder().max_size(8).build(manager)).await??;

    Ok(pool)
}
