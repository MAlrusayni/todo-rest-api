use crate::schema::todos;
use chrono::NaiveDateTime;
use diesel_derive_enum::DbEnum;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Insertable, Queryable, Identifiable, Selectable)]
pub struct Todo {
    pub id: i32,
    pub title: String,
    pub done: bool,
    pub created_at: NaiveDateTime,
}

#[derive(Deserialize, Serialize, AsChangeset)]
#[diesel(table_name = todos)]
pub struct UpdateTodo {
    pub title: String,
    pub done: bool,
}
