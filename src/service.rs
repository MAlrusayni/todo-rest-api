use crate::{
    database::Pool,
    model::{Todo, UpdateTodo},
    repository::Repository,
};
use anyhow::Result;

pub struct TodoService {
    repo: Repository,
}

impl TodoService {
    pub fn new(db_pool: Pool) -> Self {
        Self {
            repo: Repository::new(db_pool),
        }
    }

    pub async fn get_todo_by_id(&self, id: i32) -> Result<Option<Todo>> {
        self.repo.get_by_id(id).await.map_err(From::from)
    }

    pub async fn get_all_todos(&self) -> Result<Vec<Todo>> {
        self.repo.get_all().await.map_err(From::from)
    }

    pub async fn create_new_todo(&self, todo: Todo) -> Result<Todo> {
        self.repo.insert_todo(todo).await.map_err(From::from)
    }

    pub async fn delete_todo_by_id(&self, id: i32) -> Result<Todo> {
        self.repo.delete_by_id(id).await.map_err(From::from)
    }

    pub async fn update_old_todo(&self, todo_id: i32, todo: UpdateTodo) -> Result<Todo> {
        self.repo
            .update_todo(todo_id, todo)
            .await
            .map_err(From::from)
    }
}
