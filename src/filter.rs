use std::convert::Infallible;

use tracing::error;
use warp::{hyper::StatusCode, reject::Reject, reply::Reply, Filter, Rejection};

use crate::{
    database::Pool,
    model::{Todo, UpdateTodo},
    service::TodoService,
};

pub(crate) fn api(db_pool: Pool) -> impl Filter<Extract = impl Reply, Error = Infallible> + Clone {
    let base = warp::path!("todo" / ..);

    let get_todo_by_id = base
        .and(warp::get())
        .and(warp::path::param())
        .and(with_service(db_pool.clone()))
        .and_then(get_todo);

    let get_all_todos = base
        .and(warp::get())
        .and(with_service(db_pool.clone()))
        .and_then(get_all_todos);

    let create_todo = base
        .and(warp::post())
        .and(warp::body::json())
        .and(with_service(db_pool.clone()))
        .and_then(create_todo);

    let delete_todo = base
        .and(warp::path::param())
        .and(warp::delete())
        .and(with_service(db_pool.clone()))
        .and_then(delete_todo);

    let update_todo = base
        .and(warp::put())
        .and(warp::path::param())
        .and(warp::body::json())
        .and(with_service(db_pool.clone()))
        .and_then(update_todo);

    get_todo_by_id
        .or(get_all_todos)
        .or(create_todo)
        .or(delete_todo)
        .or(update_todo)
        .recover(handler_errors)
}

fn with_service(
    db_pool: Pool,
) -> impl Filter<Extract = (TodoService,), Error = Infallible> + Clone {
    warp::any().map(move || TodoService::new(db_pool.clone()))
}

async fn handler_errors(err: Rejection) -> Result<impl Reply, Infallible> {
    let code;
    let message;

    if let Some(api_err) = err.find::<ApiError>() {
        code = match api_err {
            &ApiError::TodoIdNotFound(_) => StatusCode::NOT_FOUND,
            &ApiError::InternalServerError(_) => StatusCode::INTERNAL_SERVER_ERROR,
        };
        message = api_err.to_string();
        error!("error: {api_err:?}");
    } else {
        code = StatusCode::INTERNAL_SERVER_ERROR;
        message = StatusCode::INTERNAL_SERVER_ERROR.to_string();
    };

    Ok(warp::reply::with_status(message, code))
}

// === handlers

async fn get_todo(id: i32, service: TodoService) -> Result<impl Reply, Rejection> {
    let todo = service.get_todo_by_id(id).await.map_err(ApiError::from)?;

    match todo {
        Some(todo) => Ok(warp::reply::json(&todo)),
        None => Err(warp::reject::custom(ApiError::TodoIdNotFound(id))),
    }
}

async fn get_all_todos(service: TodoService) -> Result<impl Reply, Rejection> {
    service
        .get_all_todos()
        .await
        .map(|todos| warp::reply::json(&todos))
        .map_err(ApiError::from)
        .map_err(warp::reject::custom)
}

async fn create_todo(todo: Todo, service: TodoService) -> Result<impl Reply, Rejection> {
    service
        .create_new_todo(todo)
        .await
        .map(|todo| warp::reply::json(&todo))
        .map_err(ApiError::from)
        .map_err(warp::reject::custom)
}

async fn delete_todo(id: i32, service: TodoService) -> Result<impl Reply, Rejection> {
    service
        .delete_todo_by_id(id)
        .await
        .map(|todo| warp::reply::json(&todo))
        .map_err(ApiError::from)
        .map_err(warp::reject::custom)
}

async fn update_todo(
    todo_id: i32,
    todo: UpdateTodo,
    service: TodoService,
) -> Result<impl Reply, Rejection> {
    service
        .update_old_todo(todo_id, todo)
        .await
        .map(|todo| warp::reply::json(&todo))
        .map_err(ApiError::from)
        .map_err(warp::reject::custom)
}

#[derive(thiserror::Error, Debug)]
pub enum ApiError {
    #[error("Ops, Internal error happend")]
    InternalServerError(#[from] anyhow::Error),
    #[error("Oh no!, I cannot find todo with id {0}")]
    TodoIdNotFound(i32),
}

impl warp::reject::Reject for ApiError {}
