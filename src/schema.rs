table! {
    todos (id) {
        id -> Int4,
        title -> Varchar,
        done -> Bool,
        created_at -> Timestamp,
    }
}
