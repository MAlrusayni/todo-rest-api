-- Your SQL goes here

create table todos (
    id serial primary key,
    title varchar not null,
    done boolean not null,
    created_at timestamp not null
)
